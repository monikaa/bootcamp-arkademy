function is_username_valid(username) {
  var Regex = /^[a-zA-Z][a-zA-Z0-9]{5,9}$/ ;
    return Regex.test(username) ;
}
function is_password_valid(password) {
  var Regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.*[@])(?=.{8,})/ ;
    return Regex.test(password) ;
}

console.log(is_username_valid('@sony') ? 'benar' : 'salah');
console.log(is_username_valid('Ayu99v') ? 'benar' : 'salah');
console.log(is_password_valid('p@ssW0rd#') ? 'benar' : 'salah');
console.log(is_password_valid('C0d3YourFuture!#') ? 'benar' : 'salah');