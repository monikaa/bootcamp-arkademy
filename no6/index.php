<?php
include("koneksi.php");
?><!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<br><br><br><br><br><br><br><br><br>
<div class="container">
<div align="right">
<a href="#" class="btn btn-warning btn-lg btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">ADD</a></a></div>
<div class="modal fade" id="orangeModalSubscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="btn-warning modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">ADD DATA</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
      <form class="form-horizontal" action="add.php" method="post">
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="form3" name="nama" class="form-control validate" placeholder="Name">
          <br><br>
        </div>

        <div class="md-form">
          <i class="fas fa-envelope prefix grey-text"></i>
          <select name="work" class="browser-default custom-select form-control validate">
          <?php
          $query1= mysqli_query($kon," SELECT * FROM work ");
          if (!$query1) {
              printf("Error: %s\n", mysqli_error($kon));
              exit();
          }
          WHILE($hasil1=mysqli_fetch_array($query1,MYSQLI_NUM)){
          ?>
  <option name="work" value="<?php echo $hasil1[0];?>"><?php echo $hasil1[1];?></option>
          <?php
        }
        ?>
</select>

          <br><br>
        </div>
        <div class="md-form">
          <i class="fas fa-envelope prefix grey-text"></i>
          <select name="salary" class="browser-default custom-select form-control validate">
          <?php
          $query21= mysqli_query($kon," SELECT * FROM salary  ");
          if (!$query21) {
              printf("Error: %s\n", mysqli_error($kon));
              exit();
          }
          WHILE($hasil2=mysqli_fetch_array($query21,MYSQLI_NUM)){
          ?>
  <option name="salary" value="<?php echo $hasil2[0];?>"><?php echo "Rp ".number_format($hasil2[1]);?></option>
          <?php
        }
        ?>
</select>

          <br><br>
        </div>
        
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button name="add" class="btn btn-warning">ADD</button>
      </div>
    </div>
    </form>
    <!--/.Content-->
  </div>
</div>
  <div class="table-responsive">          
  <table class="table">
    <thead >
      <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Work</th>
        <th>Salary</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <?php
      $no=1;
      $query= mysqli_query($kon," SELECT n.name, w.name, s.salary FROM name n, work w, salary s 
      WHERE n.id_work=w.id_work AND n.id_salary=s.id_salary AND w.id_salary=s.id_salary ");
      if (!$query) {
          printf("Error: %s\n", mysqli_error($kon));
          exit();
      }
      while($hasil=mysqli_fetch_array($query,MYSQLI_NUM))
      {
      
      ?>
        <td><?php echo $no;?></td>
        <td><?php echo $hasil[0];?></td>
        <td><?php echo $hasil[1];?></td>
        <td><?php echo "Rp ".number_format($hasil[2]);?></td>
        <td ><a name="hapus" href="<?php echo "del.php?nama=".$hasil[0];?>"><image name="hapus" height=30px width=30px src="icon/icon_d.png" ></a>
        <a href="<?php echo "edit.php?id=".$hasil[0];?>" data-toggle="modal" data-target="#orangeModalSubscription"><image height=25px width=25px src="icon/icon_p.png" ></a>
        <div class="modal fade" id="orangeModalSubscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document">
   
        </td>
      </tr>
      <?php
        $no++;}
        ?>
    </tbody>
  </table>
  </div>
</div>

</body>
</html>
