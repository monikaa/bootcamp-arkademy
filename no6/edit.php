 <?php
 include"koneksi.php";
 $id=$_REQUEST["id"];
 ?>
 <html>
 <body>
 <!--Content-->
 <div class="btn-warning modal-header text-center">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2">EDIT DATA</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
      <form class="form-horizontal" action="editaksi.php" method="post">
      <?php
          $query1= mysqli_query($kon," SELECT n.name, w.name, s.salary FROM name n, work w, salary s 
          WHERE n.id_work=w.id_work AND n.id_salary=s.id_salary AND w.id_salary=s.id_salary AND n.name='$id'");
          if (!$query1) {
              printf("Error: %s\n", mysqli_error($kon));
              exit();
          }
          WHILE($hasil1=mysqli_fetch_array($query1,MYSQLI_NUM)){
          ?>
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="form3" name="nama" class="form-control validate" value="<?php echo $hasil1[0];?>">
          <br><br>
        </div>

        <div class="md-form">
          <i class="fas fa-envelope prefix grey-text"></i>
          <select name="work" class="browser-default custom-select form-control validate">
          <?php
       $query_work="SELECT * FROM work";
       $sql_work=mysqli_query($kon,$query_work);
       while($data_work=mysqli_fetch_array($sql_work,MYSQLI_NUM)) {
        if ($hasil1[1]==$data_work[1]) {
         $select="selected";
        }else{
         $select="";
        }
        echo "<option $select>".$data_work[1]."</option>";
       }
      ?>      
</select>

          <br><br>
        </div>
        <div class="md-form">
          <i class="fas fa-envelope prefix grey-text"></i>
          <select name="salary" class="browser-default custom-select form-control validate">
          <?php
       $query_salary="SELECT * FROM salary";
       $sql_salary=mysqli_query($kon,$query_salary);
       while($data_salary=mysqli_fetch_array($sql_salary,MYSQLI_NUM)) {
        if ($hasil1[2]==$data_salary[1]) {
         $select="selected";
        }else{
         $select="";
        }
        echo "<option $select>".$data_salary[1]."</option>";
       }
      ?>     
        </select>

          <br><br>
        </div>
        
      </div>
      
      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button name="edit" class="btn btn-warning"><a href="<?php echo "editaksi.php?id=".$hasil1[0]."work=".$hasil1[1];?>">EDIT</a></button>
      </div>
      <?php
        }
        ?>
    </div>
    </form>
    <!--/.Content-->
</body>
</html>
