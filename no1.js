function getBiodata() {
    return {
      name: 'Monika Agustiningtias',
      age: 21,
      address: 'Jl. Lapangan Dharmawangsa No 16, Surabaya',
      hobbies: ['Membaca', 'Nonton Film'], //Array,
      is_married: false, //Boolean
      list_school: [{
        highSchoolName: 'SMAN 1 Pekutatan',
        year_in: 2013,
        year_out:2016,
        major:'MIA'
      },{
        universityName: 'Universitas Airlangga',
        year_in: 2016,
        year_out:2019,
        major:'D3 Sistem Informasi'
      }], //Array Of Object
      skills: [{
        name: 'Web Programming',
        level: 'Beginner'
      },{
        name: 'UI / UX Design',
        level: 'Beginner'
      },{
        name: 'Multimedia',
        level: 'Beginner'
      },{
        name: 'Mobile Programming',
        level: 'Beginner'
      }], // Array Of Object
      interest_in_coding: true //Boolean
    }
  }
let profile = getBiodata();

let name = profile.name;
let age = profile.age;
let address = profile.address;
let hobbies = profile.hobbies;
let is_married = profile.is_married;
let list_school = profile.list_school;
let skills = profile.skills;
let interest_in_coding = profile.interest_in_coding;
// print json yang telah di convert ke type String
console.log(JSON.stringify('name:'+name));
console.log(JSON.stringify('age:'+age));
console.log(JSON.stringify('address:'+address));
console.log(JSON.stringify('hobbies:'+hobbies));
console.log(JSON.stringify('is_married:'+is_married));
console.log(JSON.stringify(list_school));
console.log(JSON.stringify(skills));
console.log(JSON.stringify(interest_in_coding));